package com.sd.elibrary.controllers;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sd.elibrary.entities.Book;
import com.sd.elibrary.entities.BookGrade;
import com.sd.elibrary.entities.Client;
import com.sd.elibrary.services.BookService;
import com.sd.elibrary.services.ClientService;
import com.sd.elibrary.services.EntryService;
import com.sd.elibrary.services.LoginService;
import com.sd.elibrary.services.MailService;

@CrossOrigin(origins = {"http://localhost:4200"},maxAge = 4800,allowCredentials = "true")  
@RequestMapping("/book")
@RestController
public class
BookController {
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	ClientService clientService;
	
	@Autowired
	BookService bookService;
	
	@Autowired
	EntryService entryService;
	
	MailService mailService = new MailService();

	@RequestMapping(value="/list", method=RequestMethod.GET)
	public ResponseEntity<?> listBooks(HttpServletRequest request) {
		if (loginService.checkLoggedIn(request)) {
			return new ResponseEntity<List<Book>>(bookService.findAllBooks(), HttpStatus.OK);
		}
		return new ResponseEntity<String>("unauthorized", HttpStatus.UNAUTHORIZED);
	}
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public String postBook(@RequestBody Book book)
	{
		bookService.saveBook(book);
		List<Client> clients = clientService.getAllClients();
		for(Client client: clients)
		{
			if(!client.getUsername().equals("admin"))
			{
				String message = "The book " + book.getTitle() + " has been added in the databse.\n Feel free to read it. \n"
						+ "Hope you will enjoy it!";
				mailService.sendMail(client.getEmail(), "Book Added", message);
			}
		}
		return "" + book.getId();
	}

	@RequestMapping(value="/query", method=RequestMethod.GET)
	public ResponseEntity<?> listBooksByCriterion(@RequestParam Map<String, String> allParameters, HttpServletRequest request) {
		if (loginService.checkLoggedIn(request)) {
			return new ResponseEntity<List<Book>>(bookService.findBooks(allParameters), HttpStatus.OK);
		}
		return new ResponseEntity<String>("unauthorized", HttpStatus.UNAUTHORIZED);
	}
	
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public String deleteBook(@RequestBody Book book) {
			bookService.deleteBookById(book.getId());
			return "" + book.getId();
			
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public String updateGradeOfBook(@RequestBody BookGrade data) {
			
			Float gradeReceived = data.getGrade();
			Integer bookReceivedId = data.getBookId();
			Book originalBook = bookService.findById(bookReceivedId);
			
			Float originalGrade = originalBook.getGrade();
			Integer originalNrReviews = originalBook.getNrReviews();
			Integer newNrReviews = originalNrReviews + 1;
			
			Float newGrade = ( originalNrReviews * originalGrade + gradeReceived)/newNrReviews;
			
			originalBook.setGrade(newGrade);
			originalBook.setNrReviews(newNrReviews);
			
			bookService.saveBook(originalBook);
			
			return "" + originalBook.getId();
			
	}
	
	
	
}
