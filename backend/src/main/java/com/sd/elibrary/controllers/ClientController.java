package com.sd.elibrary.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sd.elibrary.entities.Client;
import com.sd.elibrary.services.ClientService;
import com.sd.elibrary.services.LoginService;

@CrossOrigin(origins = {"http://localhost:4200"},maxAge = 4800,allowCredentials = "true") 
@RequestMapping("/client")
@RestController
public class ClientController {
	
	@Autowired
	ClientService clientService;
	
	@Autowired
	LoginService loginService;
	
	@RequestMapping(value="/details/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> getClientById(@PathVariable("id") int clientId, HttpServletRequest request) {
		if (loginService.loggedInClientId(request) == clientId) {
			Client  resultClient = this.clientService.getClientById(clientId);
			return new ResponseEntity<Client>(resultClient, HttpStatus.OK);
		}
		return new ResponseEntity<String>("unauthorized", HttpStatus.UNAUTHORIZED);
	}
	
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public ResponseEntity<?> listClients(HttpServletRequest request) {
		//if (loginService.checkLoggedIn(request)) {
			return new ResponseEntity<List<Client>>(clientService.getAllClients(), HttpStatus.OK);
//		}
//		return new ResponseEntity<String>("unauthorized", HttpStatus.UNAUTHORIZED);
	}
	
	
	@RequestMapping(value="/signup", method=RequestMethod.POST)
	public String postClient(@RequestBody Client client)
	{
		this.clientService.addClient(client);
		return "" + client.getId();
	}
}
