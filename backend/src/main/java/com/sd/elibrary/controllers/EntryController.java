package com.sd.elibrary.controllers;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sd.elibrary.entities.Entry;
import com.sd.elibrary.services.BookService;
import com.sd.elibrary.services.EntryService;
import com.sd.elibrary.services.LoginService;

@CrossOrigin(origins = {"http://localhost:4200"},maxAge = 4800,allowCredentials = "true")  
@RequestMapping("/entry")
@RestController
public class EntryController {
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	BookService bookService;
	
	@Autowired
	EntryService entryService;
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public String postBook(@RequestBody Entry entry)
	{
		entryService.saveEntry(entry);
		return "" + entry.getId();
	}

	@RequestMapping(value="/list", method=RequestMethod.GET)
	public ResponseEntity<?> listEntries(HttpServletRequest request) {
		if (loginService.checkLoggedIn(request)) {
			return new ResponseEntity<List<Entry>>(entryService.findAllEntries(), HttpStatus.OK);
		}
		return new ResponseEntity<String>("unauthorized", HttpStatus.UNAUTHORIZED);
	}

	@RequestMapping(value="/query", method=RequestMethod.GET)
	public ResponseEntity<?> listEntryByCriterion(@RequestParam Map<String, String> allParameters, HttpServletRequest request) {
		if (loginService.checkLoggedIn(request)) {
			return new ResponseEntity<List<Entry>>(entryService.findEntries(allParameters), HttpStatus.OK);
		}
		return new ResponseEntity<String>("unauthorized", HttpStatus.UNAUTHORIZED);
	}
	
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public String deleteEntry(@RequestBody Entry entry) {
			entryService.deleteEntryById(entry.getId());
			return "" + entry.getId();
			
	}
	
}

