package com.sd.elibrary.controllers;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sd.elibrary.entities.Client;
import com.sd.elibrary.services.LoginService;

@CrossOrigin(origins = {"http://localhost:4200"},maxAge = 4800,allowCredentials = "true") 
@RestController
public class LoginController {
	
	@Autowired
	LoginService loginService;
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(@RequestBody Client client, HttpServletResponse response) {
		LoginService.AuthResult result = loginService.tryAuthenticate(client);
		if (result.isOk()) {
			response.addCookie(new Cookie("authtoken", result.getAuthToken()));
			return "" + result.getClientId();
		}
		return "-1";
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public String logout(@RequestBody Client client,HttpServletResponse response) {	
		if(client.getId()!=-1)
		{
			loginService.logout(client.getId());
			return "1";
		}
		return "-1";
	}
}
