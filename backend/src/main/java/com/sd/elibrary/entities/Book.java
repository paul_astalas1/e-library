package com.sd.elibrary.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "book")
public class Book implements java.io.Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private String title;
    private String author;
    private String publishingHouse;
    private String genre;
    private String link;
    private Float grade;
    private Integer nrReviews;



    // Cand Book instantiat in mod miraculos, e.g argumentul la BookController.postBook(), acest constructor este folosit
    public Book() {
		nrReviews = 0;
		grade = 0f;
    }

    @Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


    @Column(name = "title", nullable = false, length = 200)
    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    @Column(name = "author", nullable = false, length = 200)
    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Column(name = "publishingHouse", nullable = false, length = 200)
    public String getPublishingHouse() {return this.publishingHouse;}

    public void setPublishingHouse(String publishingHouse) {this.publishingHouse = publishingHouse;}


    @Column(name = "genre", nullable = false, length = 200)
    public String getGenre() {return this.genre;}

    public void setGenre(String genre) {this.genre = genre;}


    @Column(name = "link", nullable = false, length = 200)
    public String getLink() {return this.link;}

    public void setLink(String link) {this.link = link;}


    @Column(name = "grade", nullable = true)
    public Float getGrade() {
        return this.grade;
    }

    public void setGrade(Float grade) {
        this.grade = grade;
    }
    
    @Column(name = "nrReviews", nullable = false)
    public Integer getNrReviews() {
        return this.nrReviews;
    }
    
    public void setNrReviews(Integer nrReviews) {
        this.nrReviews = nrReviews;
    }
}
