package com.sd.elibrary.entities;


import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "client")
public class Client implements java.io.Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private String username;
    private String password;
    private String firstname;
    private String lastname;
    private String favouriteGenre;
    private String email;



    public Client() {
    }

    public Client(Integer id,String username, String password, String firstname, String lastname, String favouriteGenre, String email ) {
        super();
        this.id=id;
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.favouriteGenre = favouriteGenre;
        this.email = email;
    }

    @Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

    @Column(name = "username", nullable = false, length = 200)
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    @Column(name = "password", nullable = false, length = 200)
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Column(name = "firstname", nullable = false, length = 200)
    public String getFirstname() {return this.firstname;}

    public void setFirstname(String firstname) {this.firstname = firstname;}


    @Column(name = "lastname", nullable = false, length = 200)
    public String getLastname() {return this.lastname;}

    public void setLastname(String lastname) {this.lastname = lastname;}


    @Column(name = "favouriteGenre", nullable = false, length = 200)
    public String getFavouriteGenre() {return this.favouriteGenre;}

    public void setFavouriteGenre(String favouriteGenre) {this.favouriteGenre = favouriteGenre;}


    @Column(name = "email", nullable = false, length = 200)
    public String getEmail() {return this.email;}

    public void setEmail(String email) {this.email = email;}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((favouriteGenre == null) ? 0 : favouriteGenre.hashCode());
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (favouriteGenre == null) {
			if (other.favouriteGenre != null)
				return false;
		} else if (!favouriteGenre.equals(other.favouriteGenre))
			return false;
		if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
    
    

}