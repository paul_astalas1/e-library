package com.sd.elibrary.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "entry")
public class Entry {
	
	private int id;
	private String username;
	private String operation;
	private String book;
	private Date date;
	
	private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public Entry() {}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name = "username", nullable = false, length = 200)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "operation", nullable = false, length = 200)
	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	@Column(name = "book", nullable = false, length = 200)
	public String getBook() {
		return book;
	}

	public void setBook(String book) {
		this.book = book;
	}

	@Column(name = "date", nullable = false, length = 200)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
	public static Date createDate(String dateAsString) 
	{
		try {
			return simpleDateFormat.parse(dateAsString);
		} catch (ParseException e) {
			e.printStackTrace();
			System.out.println("creare data esuata");
			return null;
			
		}
	}
	
	public String convertDateToString(Date date)
	{
		return simpleDateFormat.format(date);
	}
	

}
