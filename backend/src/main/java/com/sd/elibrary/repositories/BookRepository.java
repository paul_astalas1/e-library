package com.sd.elibrary.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import com.sd.elibrary.entities.Book;

@Transactional
public interface BookRepository extends JpaRepository<Book, Integer>{
	public List<Book> findAll();
	
	public List<Book> findById(int id);
	public List<Book> findByTitle(String title);
	public List<Book> findByAuthor(String author);
	public List<Book> findByPublishingHouse(String publishingHouse);
	public List<Book> findByGenre(String genre);
	
	@Modifying
	@Transactional
    public void deleteById(int id);
	
}
