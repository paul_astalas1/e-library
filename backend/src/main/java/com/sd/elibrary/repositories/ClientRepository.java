package com.sd.elibrary.repositories;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sd.elibrary.entities.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Integer>{
	
	public Client findByUsernameAndPassword(String username, String password);
	public Client findById(int id);
	public List<Client> findAll();
}
