package com.sd.elibrary.repositories;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import com.sd.elibrary.entities.Entry;

public interface EntryRepository extends JpaRepository<Entry, Integer>{
	
public List<Entry> findAll();
	
	public List<Entry> findById(int id);
	public List<Entry> findByUsername(String username);
	public List<Entry> findByOperation(String operation);
	public List<Entry> findByBook(String book);
	public List<Entry> findByDate(Date date);
	
	@Modifying
	@Transactional
    public void deleteById(int id);

}
