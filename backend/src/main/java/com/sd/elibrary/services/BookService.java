package com.sd.elibrary.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sd.elibrary.entities.Book;
import com.sd.elibrary.repositories.BookRepository;

@Service
public class BookService {

	@Autowired
	private BookRepository bookRepo;
	
	// Maps search criteria (field names such as 'title', 'author', 'genre', 'publishingHouse') to search functions
	private Map<String, Function<String, List<Book>>> searchFunctions;
	
	public BookService() {
		searchFunctions = new HashMap<>();
		searchFunctions.put("title", new Function<String, List<Book>>() {
			@Override
			public List<Book> apply(String title) {
				return bookRepo.findByTitle(title);
			}
		});
		searchFunctions.put("author", new Function<String, List<Book>>() {
			@Override
			public List<Book> apply(String author) {
				return bookRepo.findByAuthor(author);
			}
		});
		searchFunctions.put("genre", new Function<String, List<Book>>() {
			@Override
			public List<Book> apply(String genre) {
				return bookRepo.findByGenre(genre);
			}
		});
		searchFunctions.put("publishingHouse", new Function<String, List<Book>>() {
			@Override
			public List<Book> apply(String publishingHouse) {
				return bookRepo.findByPublishingHouse(publishingHouse);
			}
		});
	}
	
	public List<Book> findBooks(Map<String, String> queryParams) {
		List<Book> books = null;
		String appliedCriterion = "";
		for (String searchCriterion : searchFunctions.keySet()) 
		{
			String criterionValue = queryParams.get(searchCriterion);
			if (criterionValue != null && !criterionValue.equals("")) {
				// we only search by the first available criterion
				if (books == null) {
					books = searchFunctions.get(searchCriterion).apply(criterionValue);
					appliedCriterion = searchCriterion;
				}
				else {
					Logger.getLogger(this.getClass().getName()).warn("Query parameters contain multiple search criteria. Only searching by " + appliedCriterion + ".");
				}
			}
		}
		return books;
	}
	
	public void saveBook(Book book)
	{
		this.bookRepo.save(book);
	}
	
	public Book findById(int id)
	{
		return bookRepo.findById(id).get(0);
	}
	
	public void deleteBookById(int id)
	{
		bookRepo.deleteById(id);
	}
	
	public List<Book> findAllBooks()
	{
		return bookRepo.findAll();
	}
}
