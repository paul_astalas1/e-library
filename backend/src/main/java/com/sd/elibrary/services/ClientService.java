package com.sd.elibrary.services;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sd.elibrary.entities.Client;
import com.sd.elibrary.repositories.ClientRepository;

@Service
public class ClientService {

	@Autowired
	private ClientRepository clientRepo;
	
	public Client getClientById(int clientId)
	{
		return this.clientRepo.findById(clientId);
	}
	
	public List<Client> getAllClients()
	{
		return this.clientRepo.findAll();
	}
	
	public void addClient(Client client)
	{
		this.clientRepo.save(client);
	}
	

}