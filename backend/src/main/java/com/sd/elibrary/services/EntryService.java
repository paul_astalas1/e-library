package com.sd.elibrary.services;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sd.elibrary.entities.Entry;
import com.sd.elibrary.repositories.EntryRepository;

@Service
public class EntryService {
	
	@Autowired
	EntryRepository entryRepo;
	
	private Map<String, Function<String, List<Entry>>> searchFunctions;
	
	public EntryService() {
		searchFunctions = new HashMap<>();
		searchFunctions.put("username", new Function<String, List<Entry>>() {
			@Override
			public List<Entry> apply(String username) {
				return entryRepo.findByUsername(username);
			}
		});
		searchFunctions.put("operation", new Function<String, List<Entry>>() {
			@Override
			public List<Entry> apply(String operation) {
				return entryRepo.findByOperation(operation);
			}
		});
		searchFunctions.put("book", new Function<String, List<Entry>>() {
			@Override
			public List<Entry> apply(String book) {
				return entryRepo.findByBook(book);
			}
		});
		searchFunctions.put("date", new Function<String, List<Entry>>() {
			@Override
			public List<Entry> apply(String date) {
				Date convertedDate = Entry.createDate(date);
				return entryRepo.findByDate(convertedDate);
			}
		});
	}
	
	public List<Entry> findEntries(Map<String, String> queryParams) {
		List<Entry> entries = null;
		String appliedCriterion = "";
		for (String searchCriterion : searchFunctions.keySet()) 
		{
			String criterionValue = queryParams.get(searchCriterion);
			if (criterionValue != null && !criterionValue.equals("")) {
				// we only search by the first available criterion
				if (entries == null) {
					entries = searchFunctions.get(searchCriterion).apply(criterionValue);
					appliedCriterion = searchCriterion;
				}
				else {
					return entries;
					//Logger.getLogger(this.getClass().getName()).warn("Query parameters contain multiple search criteria. Only searching by " + appliedCriterion + ".");
				}
			}
		}
		return entries;
	}
	
	public void saveEntry(Entry Entry)
	{
		this.entryRepo.save(Entry);
	}
	
	public Entry findById(int id)
	{
		return entryRepo.findById(id).get(0);
	}
	
	public void deleteEntryById(int id)
	{
		entryRepo.deleteById(id);
	}
	
	public List<Entry> findAllEntries()
	{
		return entryRepo.findAll();
	}

}
