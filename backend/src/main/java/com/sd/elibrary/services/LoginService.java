package com.sd.elibrary.services;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sd.elibrary.entities.Client;
import com.sd.elibrary.repositories.ClientRepository;

@Service
public class LoginService {

	@Autowired
	private ClientRepository clientRepo;
	
	private Map<String, Integer> tokenToClientId = new HashMap<>();
	private Random random = new Random();
	
	public AuthResult tryAuthenticate(Client potentialClient) {
		Client actualClient = clientRepo.findByUsernameAndPassword(potentialClient.getUsername(),  potentialClient.getPassword());
		if (actualClient == null) {
			return new AuthResult();
		}
		//TODO ce se intampla daca clientu e logat deja?
		String token = "" + random.nextLong();
		while (tokenToClientId.containsKey(token)) {
			token = "" + random.nextLong();
		}
		tokenToClientId.put(token, actualClient.getId());
		return new AuthResult(actualClient.getId(), token);
	}


	public boolean checkLoggedIn(HttpServletRequest request) {
		if (request.getCookies() != null) {
			for (Cookie c : request.getCookies()) {
				if (c.getName().equals("authtoken")) {
					return tokenToClientId.containsKey(c.getValue());
				}
			}
		}
		return false;
	}
	
	public int loggedInClientId(HttpServletRequest request) {
		Integer result = null;
		if (request.getCookies() != null) {
			for (Cookie c : request.getCookies()) {
				if (c.getName().equals("authtoken")) {
					result = tokenToClientId.get(c.getValue());
				}
			}
		}
		
		return (null == result) ? -1 : result;
	}
	
	public void logout(Integer clientId)
	{
		tokenToClientId.values().remove(clientId);
	}

	public class AuthResult {
		private int clientId;
		private String authToken;

		/**
		 * Builds an unsuccessful authentication result
		 */
		public AuthResult() {
			clientId = -1;
		}

		public AuthResult(int clientId, String authToken) {
			this.clientId = clientId;
			this.authToken = authToken;
		}

		public int getClientId() {
			return clientId;
		}

		public String getAuthToken() {
			return authToken;
		}

		public boolean isOk() {
			return clientId >=0;
		}
	}

}
