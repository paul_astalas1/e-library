import { Component, OnInit } from '@angular/core';
import { ClientModel } from '../models/client.model';
import { BookModel } from '../models/book.model';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../rest_service/rest_service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
  providers: [DataService]
})
export class AdminComponent implements OnInit {

  private admin: ClientModel;
  private searchResultBooks: any;
  private searchResultClients: any;
  private pages: string[];
  private currentPage: string;
  private bookForUpdateGrade : BookModel;
  private curentGrade: string;

  constructor(private router: Router, private route: ActivatedRoute, private service: DataService) {
    this.admin = new ClientModel();
    this.bookForUpdateGrade = new BookModel();
    this.pages = ["literary-realism","poetry","drama","romance","fantasy","economy","science",
                    "philosophy","showBookQueryResult","showClientQueryResult","read-book"];
  }

  ngOnInit() {
    console.log("Initializing client model");

    let handleClient = (admin) => {
        this.admin = admin;
    }

    let handleError = (error) => {
        this.router.navigate(['']);
    }

    this.route.params.subscribe (
        (params) => { this.service.getClientDetails(params['id']).subscribe(handleClient, handleError); }
    )

    document.getElementById("innerSearchBox").style.display = "none";
    document.getElementById("innerAddBookBox").style.display = "none";
    document.getElementById("innerGenreBox").style.display = "none";

    this.currentPage = "poetry";

    let i;
    for (i = 0; i < this.pages.length; i++) 
    { 
      document.getElementById(this.pages[i]).style.display = "none";
    }
  }

  public logout() {
    var handleLogout = (res) => {
        if (res >= 0) {
            this.router.navigate(['login', res]);
        }
    }

    this.service.logout(this.admin).subscribe(handleLogout);
  }

  searchBooks(searchParameters) {
    let handleBooks = books => {this.searchResultBooks = books;};
    this.service.getBooksByField(
        searchParameters.type,
        searchParameters.key
    ).subscribe(handleBooks);
  } 

  deleteSelectedBook(book)
  {
    this.service.removeBook(book).subscribe((resp)=>{this.listAllBooks();});
  }

  addBook(book)
  {
    this.service.addBook(book).subscribe((resp)=>{this.listAllBooks();});
  }

  updateGrade(formValue)
  {
    this.curentGrade = formValue; // verifica folosirea acestei var.
    this.service.updateBookGrade(formValue.grade, this.bookForUpdateGrade).subscribe((resp)=>{this.listAllBooks();});
  }


  toggleVisualisationSearch()
  {
    var x = document.getElementById("innerSearchBox");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
  }

  toggleVisualisationAddBook()
  {
    var x = document.getElementById("innerAddBookBox");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
  }

  toggleVisualisationGenres()
  {
    var x = document.getElementById("innerGenreBox");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
  }

  goToLiteraryRealismPage()
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[0]).style.display = "block";
    this.currentPage = this.pages[0];
  }

  goToPoetryPage()
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[1]).style.display = "block";
    this.currentPage = this.pages[1];
  }

  goToDramaPage()
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[2]).style.display = "block";
    this.currentPage = this.pages[2];
  }

  goToRomancePage()
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[3]).style.display = "block";
    this.currentPage = this.pages[3];
  }

  goToFantasyPage()
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[4]).style.display = "block";
    this.currentPage = this.pages[4];
  }

  goToEconomyPage()
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[5]).style.display = "block";
    this.currentPage = this.pages[5];
  }

  goToSciencePage()
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[6]).style.display = "block";
    this.currentPage = this.pages[6];
  }

  goToPhilosophyPage()
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[7]).style.display = "block";
    this.currentPage = this.pages[7];
  }

  goToShowBookQueryResultPage()
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[8]).style.display = "block";
    this.currentPage = this.pages[8];
  }

  goToShowClientQueryResultPage()
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[9]).style.display = "block";
    this.currentPage = this.pages[9];
  }

  listAllBooks()
  {
    this.goToShowBookQueryResultPage();
    let handleBooks = books => {this.searchResultBooks = books;};
    this.service.getAllBooks().subscribe(handleBooks);
  }

  listAllClients()
  {
    this.goToShowClientQueryResultPage();
    let handleClients = clients => {this.searchResultClients = clients;};
    this.service.getAllClients().subscribe(handleClients);
  }

  readSelectedBook(book)
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[10]).style.display = "block";
    this.currentPage = this.pages[10];

    this.bookForUpdateGrade = book;

    document.getElementById("openPDF").innerHTML = '<object class="textArea" data=' 
                                                           + book.link +'></object>' + 
                                                           '<style>\
                                                           .textArea\
                                                           {\
                                                               margin: 0px;\
                                                               width: 1100px;\
                                                               height: 450px;\
                                                               border: 3px solid black;\
                                                           }\
                                                           </style>';
  }
}

