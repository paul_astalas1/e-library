import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../rest_service/rest_service';


@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [DataService]
})

export class LoginComponent  {

    constructor(private service: DataService, private router: Router) {
    }

    public login(user) {
        var handleLogin = (res) => {
            if (res >= 0) {
                if(user.username == "admin")
                {
                    this.router.navigate(['admin',res]);
                    console.log("here admin");
                }
                else
                {
                    this.router.navigate(['client', res]);
                }
            }
            else
            {
                document.getElementById("messageLoginFailed").innerHTML = "Wrong username or password";
            }
        }


        this.service.login(user).subscribe(handleLogin);
    }
}
