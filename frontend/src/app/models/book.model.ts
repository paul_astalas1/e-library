export class BookModel {
    title: string;
    author: string;
    publishingHouse: string;
    genre: string;
    link: string;
    grade: string;
    nrReviews: string;
}