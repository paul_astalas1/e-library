import { HttpParams, HttpClient, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class DataService {

    private jsonHeaders: HttpHeaders;

    private loginUrl: string;
    private clientDetailsUrl: string;
    private listBooksUrl: string;
    private listClientsUrl:string;
    private addBookUrl: string;
    private deleteBookUrl: string;
    private updateGradeUrl: string;
    private postClientUrl: string;
    private logoutUrl: string;
    private searchBooksUrl: string;


    constructor(private http: HttpClient) {
        this.jsonHeaders = new HttpHeaders().set('Content-Type', 'application/json');
        let baseUrl = "http://localhost:8080";
        this.loginUrl = baseUrl + "/login";

        let clientBaseUrl = baseUrl + "/client";
        this.clientDetailsUrl = clientBaseUrl + "/details";
        this.listBooksUrl = baseUrl + "/book/list";
        this.listClientsUrl = baseUrl + "/client/list";
        this.addBookUrl = baseUrl + "/book/add";
        this.deleteBookUrl = baseUrl + "/book/delete";
        this.updateGradeUrl = baseUrl + "/book/update";

        this.postClientUrl = clientBaseUrl + "/signup";
        this.logoutUrl = baseUrl + "/logout";
        this.searchBooksUrl = baseUrl + "/book/query";
    }

    public login(user) {
        return this.http.post(this.loginUrl, user, {responseType: 'text', headers: this.jsonHeaders, withCredentials: true});
    }

    public logout(user) {
        return this.http.post(this.logoutUrl,user ,{responseType: 'text', headers: this.jsonHeaders, withCredentials: true});
    }

    public updateBookGrade(grade,book)
    {
        console.log(book.id);
        return this.http.post(this.updateGradeUrl,{bookId: book.id, grade: grade} ,{responseType: 'text', headers: this.jsonHeaders, withCredentials:true});
    }

    public getClientDetails(clientId) {
        return this.http.get(this.clientDetailsUrl + "/" + clientId, {headers: this.jsonHeaders, withCredentials: true});
    }

    public getAllBooks() {
        return this.http.get(this.listBooksUrl, {headers: this.jsonHeaders, withCredentials:true});
    }

    public getAllClients() {
        return this.http.get(this.listClientsUrl, {headers: this.jsonHeaders, withCredentials:true});
    }

    public getBooksByField(fieldName, fieldValue) {
          return this.http.get(this.searchBooksUrl, {params: new HttpParams().set(fieldName, fieldValue), withCredentials:true});
    }

    public removeBook(book)
    {
        return this.http.post(this.deleteBookUrl, book, {responseType: 'text', headers: this.jsonHeaders, withCredentials: true});
    }

    public addBook(book)
    {
        return this.http.post(this.addBookUrl, book, {responseType: 'text', headers: this.jsonHeaders, withCredentials: true});
    }

    public postClient(client)
    {
        return this.http.post(this.postClientUrl, client, {responseType: 'text', headers: this.jsonHeaders, withCredentials: true});
    }
}
